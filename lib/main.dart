import 'dart:async';
import 'package:flutter/material.dart';
import 'package:MarvelWeb/services/marvel.dart';
import 'dart:convert' as convert;

import 'package:flutter_conditional_rendering/conditional.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Marvel API',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List characters;
  bool mounted = false;
  int activeIndex = 0;
  ApiMarvel apiMarvel = ApiMarvel();
  int total;
  int offset = 0;
  int gridCount = 1;
  String _searchText = "";
  bool _isSearching = false;
  Widget appBarTitle = Text('Marvel API');
  int modeView = 0;

  Icon actionIcon = new Icon(
    Icons.search,
    color: Colors.white,
  );

  final TextEditingController _searchQuery = new TextEditingController();


  @override
  Widget build(BuildContext context) {
    print(activeIndex);
    if(offset < 0 || offset == null ){
      setState(() {
        offset = 0;
      });
    }
    if(activeIndex < 0){
      setState(() {
        activeIndex = 0;
      });
    }
    if ((!mounted && activeIndex == 0) || ((activeIndex) >= characters.length)) {
      setState(() {
        offset = offset + activeIndex;
        activeIndex = 0;
      });
      getCharacters();
    }
    return Scaffold(
      appBar: AppBar(
        title: appBarTitle,
        actions: [
          IconButton(
            icon: actionIcon,
            onPressed: () {
              _displayTextField();
            },
          ),
          IconButton(
            icon: Icon(modeView == 0 ? Icons.list_alt_outlined : Icons.call_to_action),
            onPressed: () {
              setState(() {
                modeView = modeView == 0 ? 1 : 0;
              });
            },
          ),
          IconButton(
            icon: Icon(Icons.format_align_center_outlined),
            onPressed: () {
              setState(() {
                gridCount = 1;
              });
            },
          ),
          // action button
          IconButton(
            icon: Icon(Icons.vertical_split_outlined),
            onPressed: () {
              setState(() {
                gridCount = 2;
              });
            },
          ),
        ],
      ),
      body: Center(
        child: Conditional.single(
          context: context,
          conditionBuilder: (BuildContext context) => mounted == true,
            widgetBuilder: (BuildContext context) => Conditional.single(
              context: context, 
              conditionBuilder: (BuildContext context) => characters.length > 0, 
              widgetBuilder: (BuildContext context) => Container(
                child: modeView == 0 ? 
                CharacterCard(
                  chars: characters.sublist(activeIndex, activeIndex+gridCount),
                  go: go,
                  isFirst: offset == 0 && activeIndex == 0,
                  isLast: ((offset + activeIndex) == total - 2),
                  gridCount: gridCount,
                  context: context,
                ) : 
                CharacterListView(chars: characters, getMore: getMore, total: total, activeIndex: activeIndex,)
            ),
            fallbackBuilder: (BuildContext context) => Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Nenhum personagem encontrado',
                  style: Theme.of(context).textTheme.headline4,
                )
              ]
            ),
          ),
          fallbackBuilder: (BuildContext context) => Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                offset == 0 || activeIndex == 0
                          ? 'Carregando personagens...'
                          : 'Carregando mais personagens...',
                style: Theme.of(context).textTheme.headline4,
              )
            ]
          ),
        ),
      ),
    );
  }
  
  void _handleSearchStart() {
    setState(() {
      _isSearching = true;
    });
  }

  void _handleSearchEnd() {
    final textAux = _searchText;
    setState(() {
      this.actionIcon = new Icon(
        Icons.search,
        color: Colors.white,
      );
      this.appBarTitle = new Text(
        "Marvel API",
        style: new TextStyle(color: Colors.white),
      );
      _isSearching = false;
      _searchQuery.clear();
      if(textAux !='') {
        activeIndex=0;
        getCharacters();
      };
    });
  }

  _MyHomePageState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  void _displayTextField() {
    setState(() {
      if (this.actionIcon.icon == Icons.search) {
        this.actionIcon = new Icon(
          Icons.close,
          color: Colors.white,
        );
        this.appBarTitle = new TextField(
          decoration: InputDecoration(
            suffixIcon: IconButton(icon: Icon(Icons.done), color: Colors.white, onPressed: (){
              setState(() {
                offset = 0;
              });
              getCharacters();
            }),
          ),
          autofocus: true,
          controller: _searchQuery,
          style: new TextStyle(
            color: Colors.white,
          ),
        );

        _handleSearchStart();
      } else {
        _handleSearchEnd();
      }
    });
  }

  void getCharacters() async {
    setState(() {
      mounted = false;
    });
    var response = await apiMarvel.listCharacters(offset, _searchText);
    if (response.statusCode == 200) {
      var body = convert.jsonDecode(response.body);
      setState(() {
        total = body['data']['total'];
        characters = body['data']['results'];
        mounted = true;
      });
    } else {
      setState(() {
        mounted = true;
      });
      print('Request failed with status: ${response.statusCode}.');
    }
  }

  void go(int to) async {
    if (offset != 0 && activeIndex == 0 && to <= -1) {
      print(characters.length);
      setState(() {
        activeIndex = 19;
        offset = offset - 20;
        mounted = false;
      });
      await getCharacters();
    } else {
      setState(() {
        activeIndex = activeIndex + to;
      });
    }
  }

  getMore(contextA) async {
    setState(() {
      offset+=20;
    });
    await getCharacters();
    Navigator.of(contextA).pop();
  }
}

class CharacterCard extends StatelessWidget {
  CharacterCard({this.context, this.chars, this.go, this.isFirst, this.isLast, this.gridCount});
  final BuildContext context;
  final List chars;
  final Function go;
  final bool isFirst;
  final bool isLast;
  final int gridCount;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          height: 50,
          width: 50,
          child: Conditional.single(
            context: this.context,
            conditionBuilder: (BuildContext context) => this.isFirst == false,
            widgetBuilder: (BuildContext context) => FloatingActionButton(
              onPressed: () => go(-gridCount),
              tooltip: 'Anterior',
              child: Icon(Icons.arrow_left)
            ),
            fallbackBuilder: (BuildContext context) => FloatingActionButton(
              backgroundColor: Colors.grey,
              onPressed: () => {},
              tooltip: 'Anterior',
              child: Icon(Icons.arrow_left)
            )
          ),
        ),
        Expanded(
          child: Row(
            children: <Widget>[
              for(var char in chars)
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        flex: 1,
                        child: 
                        Text(char['name'].toString(),
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 20,
                          )
                        )
                      ),
                      Expanded(
                        flex: 1,
                        child: 
                        Text(char['description'].toString() == '' ? 'No Description' : char['description'].toString(),
                          style: TextStyle(
                            fontWeight: FontWeight.w300,
                            fontSize: 14,
                          )
                        )
                      ),
                      Expanded(
                        flex: 6,
                        child: SizedBox(
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(width: 1, color: Colors.white),
                              borderRadius: BorderRadius.all(Radius.circular(25))
                            ),
                            child: ClipRRect(
                              borderRadius: BorderRadius.all(Radius.circular(25)),
                              child: Image(
                                image: NetworkImage(char['thumbnail']['path'].toString() + '.' 
                                + char['thumbnail']['extension'].toString()),
                              )
                            ), 
                          ),
                        )
                      )
                    ]
                  )
                )
            ]),
        ),
        SizedBox(
          width: 50,
          height: 50,
          child: Conditional.single(
            context: this.context,
            conditionBuilder: (BuildContext context) => this.isLast == false,
            widgetBuilder: (BuildContext context) => FloatingActionButton(
              onPressed: () => go(gridCount),
              tooltip: 'Próximo',
              child: Icon(Icons.arrow_right)
            ),
            fallbackBuilder: (BuildContext context) => FloatingActionButton(
              backgroundColor: Colors.grey,
              onPressed: () => {},
              tooltip: 'Próximo',
              child: Icon(Icons.arrow_right)
            )
          ),
        ),
      ],
    );
  }
}

class CharacterListView extends StatelessWidget{
  CharacterListView({this.chars, this.getMore, this.total, this.activeIndex});
  final _controller = ScrollController(); 
  final List chars;
  final Function getMore;
  final int total;
  final activeIndex;

  Widget build(BuildContext context) {
    void _showDialog() {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // retorna um objeto do tipo Dialog
          return AlertDialog(
            title: new Text("Carregar mais personagens?"),
            content: new Text("Você deseja carregar mais personagens?"),
            actions: <Widget>[
              // define os botões na base do dialogo
              new FlatButton(
                child: new Text("Sim"),
                onPressed: () async {
                  await getMore(context);
                  _controller.position.setPixels(0);
                },
              ),
              new FlatButton(
                child: new Text("Não"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
    if(chars.length == 20 && total > activeIndex+1*20) {
      _controller.addListener(() {
      if (_controller.position.atEdge) {
        if (_controller.position.pixels != 0)
            Timer(Duration(milliseconds: 500), (){
              _showDialog();
            });
        }
      });
    }
    if(chars.length < 20 && Navigator.of(context).canPop()) {
      Navigator.of(context).pop();
    }
    return (
      ListView.separated(
        padding: const EdgeInsets.all(8),
        itemCount: chars.length,
        controller: _controller,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            height: 50,
            child: Row(
              children: <Widget> [
                Expanded(
                  flex: 1,
                  child: Image(
                    image: NetworkImage(chars[index]['thumbnail']['path'].toString() + '.' 
                    + chars[index]['thumbnail']['extension'].toString()),
                  )
                ),
                Expanded(
                  flex: 4,
                  child: Text(chars[index]['name'].toString(),
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 20,
                    )
                  )
                ),
                Expanded(
                  flex: 6,
                  child: Text(chars[index]['description'].toString(),
                    style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: 10,
                    )
                  )
                )
              ],
            )
          );
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(),
      )
    );
  }
}