import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:crypto/crypto.dart';

class ApiMarvel {
  Future<http.Response> listCharacters(int offset, String name) async {
    var timestamp = DateTime.now().millisecondsSinceEpoch;
    const PRIVATE_KEY = '2ea8a7191482b9d2eb796fa68a524e9c7298c213';
    const PUBLIC_KEY = '69cc3fea04d203d46d549b0761b6d9a4';
    String hash = textToMd5(timestamp.toString() + PRIVATE_KEY + PUBLIC_KEY);
    String url =
        'https://gateway.marvel.com/v1/public/characters?offset=$offset?limit=20&apikey=$PUBLIC_KEY&hash=$hash&ts=$timestamp';
    
    if(name != ''){
      name = name.replaceAll(' ', '-');
      url+='&nameStartsWith=$name';
    }

    var response = await http.get(url);
    return response;
  }

  String textToMd5(String text) {
    return md5.convert(convert.utf8.encode(text)).toString();
  }
}
